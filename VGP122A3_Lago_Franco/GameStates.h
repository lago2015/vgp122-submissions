#include"Definitions.h"

class GameState
{
public:
	virtual void update() = 0;
	virtual void render() = 0;
	virtual void onEnter() = 0;
	virtual void onExit() = 0;

	enum gameStates{
		MENU = 0,
		PLAY = 1,
		GAMEOVER = 2
	};
	virtual std::string getStateID() const = 0;
};
//class for avatar
class avatar
{
public :
	void Avatar(string name, string image);
};
//class for shuffling and dealing deck
class Card
{
public:
	void playerCards1(int num, string face);
	void playerCards2(int num, string face);
	int deck(int num, string face);
};
//taking in player mouse inputs!
class playerInput
{
public:
	void mouseInput(string obj);
};