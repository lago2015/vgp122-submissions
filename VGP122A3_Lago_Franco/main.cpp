#include "Definitions.h"
#include"GameStates.h"
#include "Luigi.h"
#include "Sound.h"
#include"Text.h"
#include<stdio.h>
#include"SDL_mixer.h"

//Displaying menu before playing
int showMenu(SDL_Surface*screen, TTF_Font* font)
{
	Uint32 time;
	int x, y;
	const int NumMenu = 2;
	const char* labels[NumMenu] = { "Start", "Exit" };
	SDL_Surface* menuOption[NumMenu];
	bool selected[NumMenu] = { 0, 0 };
	SDL_Color color[2] = { { 255, 255, 255 }, { 255, 0, 0 } };

	menuOption[0] = TTF_RenderText_Solid(font, labels[0], color[0]);
	menuOption[1] = TTF_RenderText_Solid(font, labels[1], color[0]);

	SDL_Rect pos[NumMenu];
	pos[0].x = screen->clip_rect.w / 2 - menuOption[0]->clip_rect.w / 2;
	pos[0].y = screen->clip_rect.h / 2 - menuOption[0]->clip_rect.h;
	pos[1].x = screen->clip_rect.w / 2 - menuOption[0]->clip_rect.w / 2;
	pos[1].y = screen->clip_rect.h / 2 + menuOption[0]->clip_rect.h;

	SDL_Event event;
	while (1)
	{
		time = SDL_GetTicks();
		while (SDL_PollEvent(&event))
		{
			switch (event.type)
			{
			case SDL_QUIT:
				SDL_FreeSurface(menuOption[0]);
				SDL_FreeSurface(menuOption[1]);
				return 1;
			case SDL_MOUSEMOTION:
				x = event.motion.x;
				y = event.motion.y;
				for (int i = 0; i < NumMenu; i += 1) {
					if (x >= pos[i].x && x <= pos[i].x + pos[i].w && y >= pos[i].y && y <= pos[i].y + pos[i].h)
					{
						if (!selected[i])
						{
							selected[i] = 1;
							SDL_FreeSurface(menuOption[i]);
							menuOption[i] = TTF_RenderText_Solid(font, labels[i], color[1]);
						}
					}
					else
					{
						if (selected[i])
						{
							selected[i] = 0;
							SDL_FreeSurface(menuOption[i]);
							menuOption[i] = TTF_RenderText_Solid(font, labels[i], color[0]);
						}
					}
				}
				break;
			case SDL_MOUSEBUTTONDOWN:
				x = event.button.x;
				y = event.button.y;
				for (int i = 0; i < NumMenu; i += 1) {
					if (x >= pos[i].x && x <= pos[i].x + pos[i].w && y >= pos[i].y && y <= pos[i].y + pos[i].h)
					{
						SDL_FreeSurface(menuOption[0]);
						SDL_FreeSurface(menuOption[1]);
						return i;
					}
				}
				break;
			case SDL_KEYDOWN:
				if (event.key.keysym.sym == SDLK_ESCAPE)
				{
					SDL_FreeSurface(menuOption[0]);
					SDL_FreeSurface(menuOption[1]);
					return 0;
				}
			}
		}
	}
}

int main(int argc, char** argv)
{
	/** Inialize Game Variables */
	bool			done = false;
	SDL_Window*		window = NULL;
	SDL_Renderer*	renderer = NULL;

	/** Initialize Event Variables */
	SDL_Event		event;

	/** Initialize timer variables */
	int				deltaT = 75;			// defines delay in time for updating game loop
	int				updatedTime = 0;			// used to determine if frame rate interval has elapsed

	/** Initialize joystick handling variables */
	SDL_Joystick*	joy = NULL;

	/** Score tracking variables */
	int				numCoins = 0;

	/** Initialize true type font variables.
	For text to be drawn, define
	- color of text
	- destination rect of location text will be drawn
	- font used
	- text surface to draw from
	*/

	// height and width are dependant on whats being rendered

	////showing menu
	//int i = showMenu(screen, font);



	/** Initialize sprite variables */
	Sprite*			background = NULL;
	Sprite*			coin = NULL;
	Sprite*			coinHUD = NULL;
	Luigi*			luigi = NULL;

	/** Initialize music and sound effect variables */






	/** Initialize SDL */
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
	{
		cerr << "Unable to init SDL: " << SDL_GetError() << endl;
		return 1;
	}
	atexit(SDL_Quit);

	/** Create Window for Project */
	window = SDL_CreateWindow("Luigi's Revenge", 100, 100, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);

	if (window == NULL)
	{
		cerr << "SDL_CreateWindow Error: " << SDL_GetError() << endl;
		return 1;
	}

	/** Create Renderer to draw to */
	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == NULL)
	{
		cerr << "SDL_CreateRenderer Error: " << SDL_GetError() << endl;
		return 1;
	}

	/** Initialize TTF Font */
	//bool loadFromRenderedText(std::string textureText, SDL_Color textColor);

	//TTF_Font *gFont = NULL;
	//SDL_Surface* textSurface = TTF_RenderText_Solid(gfont,"Fonts/Arial.ttf",SDL_Color textColor)






	/** Initialize SDL Mixer */
	//Mix_Chunk *music = NULL;
	Mix_Music *sMusic = NULL;

	//sMusic = Mix_LoadWAV("Sounds/Mario.mid");

	if (sMusic == NULL)
	{
		cout << "Something went wrong with music!" << Mix_GetError();
	}
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		cout << "SDL count not initialize! SDL Error: " << SDL_GetError();
	}






	/** Initialize sprite properties */
	background = new Sprite("Images/Mario_Level1.png", 0, 0, 7251, 480, renderer);
	coin = new Sprite("Images/Coin_Spritesheet.png", 420, 360, 28, 32, renderer);
	coinHUD = new Sprite("Images/Coin_HUD.png", 0, 0, 80, 46, renderer);
	luigi = new Luigi("Images/Luigi_Spritesheet.png", 320, 365, 32, 64, renderer);

	/** Open access to the joystick */
	joy = SDL_JoystickOpen(0);

	if (joy != NULL)
	{
		cout << "Opened Joystick 0" << endl;
		cout << "Name: " << SDL_JoystickName(0) << endl;
		cout << "Number of Axes: " << SDL_JoystickNumAxes(joy) << endl;
		cout << "Number of Buttons: " << SDL_JoystickNumButtons(joy) << endl;
		cout << "Number of Hats: " << SDL_JoystickNumHats(joy) << endl;
	}
	else
		cout << "Couldn't open Joystick 0" << endl;

	/** Open a font to use when rendering text */







	/** Set style and message for text */







	/** Load sound files */

	//music = Mix_LoadWAV("Sounds/Mario.mid");
	//if (music == NULL)
	//{
	//	cout << "Failed to lose music file!" << Mix_GetError();
	//}










	/** Begin music */


	while (!done)
	{
		//Play Music
		if (Mix_PlayingMusic()==0)
			Mix_PlayMusic(sMusic, -1);


		/** Update game loop */
		if (SDL_GetTicks() - updatedTime >= deltaT)
		{
			/** Poll for Events */
			while (SDL_PollEvent(&event))
			{
				switch (event.type)
				{
					/** Check if user tries to quit the window */
				case SDL_QUIT:
					done = true;
					break;

					/**	Check if the ESC key was pressed */
				case SDL_KEYDOWN:
					if (event.key.keysym.sym == SDLK_ESCAPE)
						done = true;
					if (event.key.keysym.sym == SDLK_SPACE)
					{
						luigi->startJump();

						/** Play Jump Sound */

					}
					break;

					/**	Check if jump button was pressed on joystick */
				case SDL_JOYBUTTONDOWN:
					if (event.jbutton.button == 1)
					{
						luigi->startJump();

						/** Play Jump Sound */

					}
					break;
				}
			}

			/** Directly read from Keyboard */
			const Uint8 *keys = SDL_GetKeyboardState(NULL);

			if (keys[SDL_SCANCODE_LEFT] && keys[SDL_SCANCODE_RIGHT])
			{
				luigi->stop();
			}
			else if ((keys[SDL_SCANCODE_LEFT]) || (SDL_JoystickGetAxis(joy, 0) < -2500))
			{
				/** Start scrolling background to right */
				if (luigi->getXPos() <= 100)
				{
					luigi->setBGScroll(true);
					background->move(10, 0);
				}
				else
					luigi->setBGScroll(false);

				/** stop scrolling background when left boundary of background is reached */
				if (background->getXPos() > 0)
				{
					luigi->setBGScroll(false);
					background->move(-10, 0);
					coin->move(-10, 0);
				}

				/** If luigi has reached far left of screen, do not walk */
				if (luigi->getXPos() > 0)
					luigi->walkLeft();
			}
			else if ((keys[SDL_SCANCODE_RIGHT]) || (SDL_JoystickGetAxis(joy, 0) > 2500))
			{
				/** Start scrolling background to left */
				if (luigi->getXPos() >= 500)
				{
					luigi->setBGScroll(true);
					background->move(-10, 0);
				}
				else
					luigi->setBGScroll(false);

				if (background->getXPos() < (SCREEN_WIDTH - background->getWidth()))
				{
					luigi->setBGScroll(false);
					background->move(10, 0);
					coin->move(10, 0);
				}

				if (luigi->getXPos() < (SCREEN_WIDTH - luigi->getWidth()))
					luigi->walkRight();
			}
			else if (keys[SDL_SCANCODE_UP])
			{
				luigi->lookUp();
			}
			else if (keys[SDL_SCANCODE_DOWN])
			{
				luigi->duck();
			}
			else
			{
				luigi->stop();
			}

			/** Check for Luigi collision with coin */
			if (CollisionDetection::isColliding(coin, luigi) && coin->isVisible())
			{
				cout << "Luigi picked a coin up\n";

				/** Play Coin Sound */


				coin->setVisible(false);
				numCoins++;


			}

			/** Update coin and other animations */
			coin->animate(0, 3);
			luigi->updateJump();

			/** Draw all sprites */
			background->draw(renderer);
			coinHUD->draw(renderer);
			coin->draw(renderer);
			luigi->draw(renderer);

			/** Draw text to renderer*/


			/** Update display */
			SDL_RenderPresent(renderer);

			/** update time */
			updatedTime = SDL_GetTicks();
		}

	}
	/** Close access to the joystick */
	if (SDL_JoystickGetAttached(joy))
		SDL_JoystickClose(joy);

	/** Free memory used for sprites */
	delete background;
	delete coinHUD;
	delete coin;
	delete luigi;

	/** Stop and free music. Free all sound effects */


	Mix_FreeChunk(music);
	music = NULL;
	Mix_Quit();





	/** Free text surfaces */




	return 0;
}