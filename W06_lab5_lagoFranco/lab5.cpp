#include<iostream>
#include<iomanip>
#include"labHead.h"
using namespace std;
/*
	ch3:3.5-3.10,3.12,3.15
	ch9:9.3,9.4,9.6,9.23
	ch11:11.6
	ch12:12.3,12.8,12.9
*/
/*
	
	3.5)a function prototype is a declaration of a function that tells the
		compiler the functions name,its return type and parameters.Function definition
		will be used by all base-class objects and by all objects of the base
		class
	3.6)a default constructor is a constructor that doesnt take agruments. This 
		calls the default constructor for each data member that is an object of
		another class. Uninitialized variables contain garbage values
	3.7)Data members are attributes that represent as variables in class definition 
		and hold data to wait to be called within main()
	3.8)a header is used to put functions that have varies stuff to call or do.
		while source-code calls the functions within the header.
	3.9)By making a program to use class string without inserting a directive is 
		by typing stuff on the screen and see what happens within.or write the code
		that inputs the directive string within your script.
	3.10)in the main function or the source code the set and get function is 
		what these functions use for calling header functions to be used.

	9.3) Scope resolution operator(::) is used to define a function outside a class 
		or when we want to use a global variable but also has a local variable with 
		same name.
*/

void main()
{
	//3.15
	/*Date mydate(5, 6, 2015);
	Date date2(4, 12, 1994);
	Date date4;
	Date *date3;
	Date *date6;
	Date date5(1, 1, 1);

	date3 = new Date(45, 45, 44545);
	mydate.display();*/

	//9.4
	Time tt;
	std:: cout<< "\nCurrent time " << std::setw(2) << std::setfill('0')

		<< tt.getTime->tm_hour << " : " << std::setw(2) << std::setfill('0')

		<< tt.getTime->tm_min << " : " << std::setw(2) << std::setfill('0')

		<< tt.getTime->tm_sec;



	system("pause");
}