#include<iostream>
#include<string>	//program uses c++ standard string class
using namespace std;

//GradeBook definition. this file presents gradebook public interface without
//revealing the implementations fo gradebooks member functions, which
//are defined in gradebook.cpp

//gradebook class definition

	
	class GradeBook
	{
	public:
	//CONSTRUCTOR intiializes courseName with string supplied as argument
	explicit GradeBook(std::string name);
	//function that SETS the name
	void setCourseName(std::string name);
	//function that GETS the name
	std::string getCourseName()const;
	//function that displays a welcome message to user
	void displayMessage()const;
	private:
	std::string courseName;	//course name for this gradebook
	};
	
GradeBook::GradeBook(string name)
:courseName(name)
{
	//empty body
}

//function to set the course name
void GradeBook::setCourseName(string name)
{
	courseName = name;	//store the course name in the object
}
string GradeBook::getCourseName()const
{
	return courseName;	//return objects courseName
}
void GradeBook::displayMessage()const
{
	//call getCourseName to get the courseName
	cout << "Welcome to this program\n" << getCourseName() << "!" << endl;
}

