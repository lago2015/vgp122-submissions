﻿using UnityEngine;
using System.Collections;

public class AIMovements : AIState {

    public enum AIState { Patrol,Chase,Escape,Attack,Turret}

    //Patrol
        public Transform[] wayPath;     //Array for way points
        private int curPoint = 0;       //current point of array position
        private float curTime = 0.0f;   //current Time
        public float waySpeed = 5.0f;   //speed of patrol
        public float wayPause = 0.0f;   //pause on each wayPoint
        public bool wayLoop = true;     //waypoints way of being looped
        public float wayLook = 6.0f;    //how far enemy can look
    
    private CharacterController control;
    private bool bDead;
    private int health;
    public float distFromPlayer;

    public GameObject bullet;  
    public AIState curState;        //Checks what state the enemy is in
    public float curSpeed=5;         //current enemy speed
    

	// Use this for initialization
	void Start () 
    {
        control = GetComponent<CharacterController>();
        GameObject objPlayer = GameObject.FindGameObjectWithTag("Player");
        playerTransform = objPlayer.transform;
	}
	
	// Update is called once per frame
	void FixedUpdate () 
    {
        destPos = playerTransform.position;   //set target position as the player position
        float distFromPlayer = Vector3.Distance(transform.position, playerTransform.position);
       // transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);
        
	   switch (curState)
       {
           case AIState.Attack: 
               UpdateAttackState(); 
               break;
           case AIState.Patrol:
               UpdatePatrolState();
               break;
           case AIState.Chase:
               UpdateChaseState(); 
               break;
           case AIState.Escape: 
               UpdateEscapeState(); 
               break;
           case AIState.Turret: 
               UpdateTurretState(); 
               break;
       }
	}
    void UpdateAttackState()
    {
        destPos = playerTransform.position;   //set target position as the player position
        float distFromPlayer = Vector3.Distance(transform.position, playerTransform.position);
      //  transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);

        curState = AIState.Attack;
        Debug.Log("Im Attacking");
    }
    void UpdatePatrolState()
    {
        destPos = playerTransform.position;   //set target position as the player position
        float distFromPlayer = Vector3.Distance(transform.position, playerTransform.position);
      //  transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);
        curState = AIState.Patrol;
        float dist = Vector3.Distance(transform.position, playerTransform.position);
        Vector3 currentPlace = wayPath[curPoint].position;
        currentPlace.y = transform.position.y;
        currentPlace.x = transform.position.x;
        Vector3 direction = currentPlace - transform.position;
        float step = curSpeed - Time.deltaTime;
        if (distFromPlayer <= 30f)
            curState = AIState.Chase;
        else if (distFromPlayer >= 20f)
            curState = AIState.Patrol;
        
        //if (distFromPlayer >= 100f)
            
    }      
       
        //    if (direction.magnitude < 1)
        //    {
        //        if (curTime == 0)
        //        {
        //            curTime = Time.time;
        //        }
        //        if ((Time.time - curTime) >= wayPause)
        //        {
        //            curPoint++;
        //            curTime = 0;
        //        }
        //    }
        //    else
        //    {
        //        Quaternion rotation = Quaternion.LookRotation(currentPlace - transform.position);
        //        transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * wayLook);
        //        control.Move(direction.normalized * step);
        //    }
        //}
        //else if (Vector3.Distance(transform.position, playerTransform.position)<=50.0f)
        //{
        //    Debug.Log("Im switching!!");
        //    curState = AIState.Chase;
        //}
        //else
        //{
        //    if(wayLoop)
        //    {
        //        curPoint = 0;
        //    }
        
           
        
    
    void UpdateChaseState()
    {
        destPos = playerTransform.position;   //set target position as the player position
        float distFromPlayer = Vector3.Distance(transform.position, playerTransform.position);
     //   transform.Translate(Vector3.forward * Time.deltaTime * curSpeed);

        curState = AIState.Chase;
        Debug.Log("Im chasing");
        if (distFromPlayer <= 15f)
            curState = AIState.Attack;
        else if (distFromPlayer >= 20f)
            curState = AIState.Chase;
            
            

    }
    void UpdateEscapeState()
    {

    }
    void UpdateTurretState()
    {

    }
    protected bool IsInCurrentRange(Vector3 pos)
    {
        
        float xPos = Mathf.Abs(pos.x - transform.position.x);
        float zPos = Mathf.Abs(pos.z - transform.position.z);

        if (xPos <= 50 && zPos <= 50)
            return true;
            return false;
    }
}

