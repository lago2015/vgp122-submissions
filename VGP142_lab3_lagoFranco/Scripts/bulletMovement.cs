﻿using UnityEngine;
using System.Collections;

public class bulletMovement : MonoBehaviour {

    //explosion effect
    public GameObject explosion;

    public float speed = 400f;
    public float lifeTime = 3.0f;
    public int damage = 30;

	// Use this for initialization
	void Start () 
    {
        Destroy(gameObject, lifeTime);
	}
	
	// Update is called once per frame
	void Update () 
    {
        transform.position += transform.up * speed * Time.deltaTime;
	}
    void onCollisionEnter(Collision col)
    {
        ContactPoint contact = col.contacts[0];
        Instantiate(explosion, contact.point, Quaternion.identity);
        Destroy(gameObject);
    }
}
