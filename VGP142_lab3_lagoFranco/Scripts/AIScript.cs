﻿using UnityEngine;
using System.Collections;

public class AIScript : MonoBehaviour {

    public Transform[] wayPath;
    public float waySpeed = 3.0f;   //speed of patrol
    public bool wayLoop = true; //if waypoints loop
    public bool inRange = false;    //if player is in range
    public float wayLook = 6.0f;    
    public float wayPause = 0.0f;   //pause on each waypoint
    public Transform player;
    public float fieldOfView = 0.0f;

    private float currentTime = 0.0f;
    private int currentPoint = 0;   //current array position
    private CharacterController character;  //AI must have a character controler attached

	// Use this for initialization
	void Start () {
        character = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
        float dist = Vector3.Distance(transform.position, player.position);
       /* while (!inRange)
        {
            GetRange();

        if (inRange)
        {
            chase();
        }
        else
            if (currentPoint < wayPath.Length)
            {
                Patrol();
            }   //patrols if the current point isnt the last point
            else
            {
                if (wayLoop)
                {
                    currentPoint = 0;
                }   //loops back to 0 if wayLoop is true
            }
            if (dist <= fieldOfView)
            {
                inRange = true;
            }
        }

        if (inRange == true)
        {
            transform.position = Vector3.MoveTowards(transform.position, player.position, waySpeed * Time.deltaTime);
        }*/
        /*GetRange();

        if (inRange)
        {
            chase();
        }
        else */if (currentPoint < wayPath.Length)
        {
            Patrol();
        }   //patrols if the current point isnt the last point
        else
        {
            if (wayLoop)
            {
                currentPoint = 0;
            }   //loops back to 0 if wayLoop is true
        }
	}

    void Patrol () {
        Vector3 currentPlace = wayPath[currentPoint].position;
        currentPlace.y = transform.position.y;
        Vector3 direction = currentPlace - transform.position;
        float step = waySpeed * Time.deltaTime;

        if (direction.magnitude < 0.5)
        {
            if (currentTime == 0)
            {
                currentTime = Time.time;
            }
            if ((Time.time - currentTime) >= wayPause)
            {
                currentPoint++;
                currentTime = 0;
            }
        }
        else
        {
            Quaternion rotation = Quaternion.LookRotation(currentPlace - transform.position);
            transform.rotation = Quaternion.Slerp(transform.rotation, rotation, Time.deltaTime * wayLook);
            character.Move(direction.normalized * step);
        }
    }

    /*void GetRange(){
        if (player.position.x < transform.position.x + 3 && player.position.x > transform.position.x - 3)
        {
            if (player.position.z < transform.position.z + 3 && player.position.z > transform.position.z - 3)
            {
                inRange = true;
                wayLoop = false;
            }
        }
        else
        {
            inRange = false;
            wayLoop = true;
        }
    }

    void chase(){
        transform.position = Vector3.MoveTowards(transform.position, player.position, waySpeed * Time.deltaTime);
    }*/
}
