﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour {

    public GameObject Bullet;

    private Transform Gun;
    private Transform bulletSpawnPoint;
    private float curSpeed, targetSpeed, rotSpeed;
    private float turrentRotSpeed = 10.0f;
    private float maxForwardSpeed = 100.0f;
    private float maxBackwardSpeed = -100.0f;

    //bullet shooting rate
    protected float shootRate = 0.5f;
    protected float elapsedTime;

	// Use this for initialization
	void Start () 
    {
	    //tank settings
        rotSpeed = 150.0f;

        //get the turrent of the tank
        Gun = gameObject.transform.GetChild(0).transform;
        bulletSpawnPoint = Gun.GetChild(0).transform;
	}
	
	// Update is called once per frame
	void Update () 
    {
        UpdateWeapon();
        UpdateControl();

        //input controls

	}
    void UpdateWeapon()
    {
        if(Input.GetMouseButtonDown(0))
        {
            elapsedTime += Time.deltaTime;
            if(elapsedTime>=shootRate)
            {
                elapsedTime = 0.0f;     //reset time
                Instantiate(Bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);      //instantiate bullet
            }
        }
    }
    void UpdateControl()
    {
        //Aim with mouse
        //Generate a plane that intersects the transform's position with an upward normal
        Plane playerPlane = new Plane(Vector3.up, transform.position + new Vector3(0, 0, 0));

        //Generate a ray from cursor position
        Ray RayCast = Camera.main.ScreenPointToRay(Input.mousePosition);

        //Determine point where the cursor ray intersects with plane
        float hitDist = 0;

        //if the ray is parallel to the plan Raycast will return false
        if(playerPlane.Raycast(RayCast,out hitDist))
        {
            //Get the point along the ray that hits the calculated distance
            Vector3 RayHitPoint = RayCast.GetPoint(hitDist);

            Quaternion targetRotation = Quaternion.LookRotation(RayHitPoint - transform.position);

            Gun.transform.rotation = Quaternion.Slerp(Gun.transform.rotation, targetRotation, Time.deltaTime * turrentRotSpeed);
        }

        transform.Translate(Vector3.up * Input.GetAxis("Horizontal") * maxForwardSpeed * Time.deltaTime);
        transform.Translate(Vector3.up*Input.GetAxis("Horizontal")*maxBackwardSpeed*Time.deltaTime);
        transform.Translate(Vector3.up * Input.GetAxis("Vertical") * maxForwardSpeed * Time.deltaTime);
        transform.Translate(Vector3.up * Input.GetAxis("Vertical") * maxBackwardSpeed * Time.deltaTime);
            //Determine current speed
            curSpeed = Mathf.Lerp(curSpeed, targetSpeed, 7.0f *
            Time.deltaTime);
            transform.Translate(Vector3.forward * Time.deltaTime *
            curSpeed);
            }
    }
    

