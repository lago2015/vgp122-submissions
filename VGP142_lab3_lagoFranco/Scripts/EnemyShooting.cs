﻿using UnityEngine;
using System.Collections;

public class EnemyShooting : MonoBehaviour {
    
    
    public float elapsedTime = 5.0f;
    public GameObject Bullet;
    public Transform gun { get; set; }
    public Transform bulletSpawnPoint { get; set; }
    public float shootRate = 3.0f;
    


	// Use this for initialization
	void Start () 
    {
        gun = gameObject.transform.GetChild(0).transform;
        bulletSpawnPoint = gun.GetChild(0).transform;
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        
        elapsedTime += Time.deltaTime;
        if (elapsedTime>=shootRate)
        {
            elapsedTime = 0;
            ShootTheJ();
        }
	}
   
    private void ShootTheJ()
    {
        //if (elapsedTime >= 3)
        //{
       
        Instantiate(Bullet, bulletSpawnPoint.position, bulletSpawnPoint.rotation);
            //elapsedTime = 0.0f;
        //}
    }
}
