﻿using UnityEngine;
using System.Collections;

public class AIState : MonoBehaviour {

    public Transform playerTransform;       //Player Transform
    protected Vector3 destPos;      //destination position
    protected float shootRate;      //bullet shooting rate
    protected float elaspedTime;    //time controlling rate of shooting

    protected virtual void Initialize() { }
    protected virtual void AIUpdate() { }
    protected virtual void AIFixedUpdate() { }


	// Use this for initialization
	void Start () 
    {
        Initialize();
	}
	
	// Update is called once per frame
	void Update () 
    {
        AIUpdate();
	}
    void FixedUpdate()
    {
        AIFixedUpdate();
    }
}
