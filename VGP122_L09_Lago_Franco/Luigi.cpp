#include "Luigi.h"

Luigi::Luigi(const char* filename, int xpos, int ypos, int width, int height, SDL_Renderer *renderer)
: Sprite(filename, xpos, ypos, width, height, renderer)
{
	this->backgroundScrolling = false;
	this->jump = new Jump();
}

Luigi::~Luigi()
{
	delete this->jump;
}

void Luigi::walkLeft()
{
	this->animate(2, 5);

	if (!this->backgroundScrolling)
		this->move(-10, 0);
}

void Luigi::walkRight()
{
	this->animate(6, 9);

	if (!this->backgroundScrolling)
		this->move(10, 0);
}

void Luigi::stop()
{
	if (this->getCurrentFrame() < 6)
		this->setCurrentFrame(5);
	else
		this->setCurrentFrame(6);
}

void Luigi::startJump()
{
	this->jump->start(this->getYPos());
}

void Luigi::updateJump()
{
	this->jump->update(this);
}

bool Luigi::getBGScroll()
{
	return this->backgroundScrolling;
}

void Luigi::setBGScroll(bool scroll)
{
	this->backgroundScrolling = scroll;
}
void Luigi::duck()
{
	bool ducking=false;
	if (this->getCurrentFrame() < 6)
		ducking = false;
	else if (this->getCurrentFrame()>5)
		ducking = true;
	if (ducking == true)
	{
		this->setCurrentFrame(10);
	}
	else if (ducking == false)
		this->setCurrentFrame(1);
}
void Luigi::lookUp()
{
	bool looking = false;
	if (this->getCurrentFrame() < 6)
		looking = false;
	else if (this->getCurrentFrame()>5)
		looking = true;
	if (looking == true)
	{
		this->setCurrentFrame(11);
	}
	else if (looking == false)
		this->setCurrentFrame(0);
	
}