#include"Definitions.h"

class GameState
{
public:
	virtual void update() = 0;
	virtual void render() = 0;
	virtual void onEnter() = 0;
	virtual void onExit() = 0;

	enum gameStates{
		MENU = 0,
		PLAY = 1,
		GAMEOVER = 2
	};
	virtual std::string getStateID() const = 0;
};
//deck of cards
class Deck
{
public :
	void Shuffle(int num, string face);
};
//player cards
class Cards
{
	void Dealing(int num, string face);
};
//player avatar for gameplay
class avatar
{

};