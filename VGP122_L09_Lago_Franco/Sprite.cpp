#include "Sprite.h"

Sprite::Sprite(const char* filename, int xpos, int ypos, int width, int height, SDL_Renderer *renderer)
{
	/** Set variables in the Sprite class*/
	this->rect.h = height;
	this->rect.w = width;
	this->rect.x = 0;
	this->rect.y = 0;
	this->xpos = xpos;
	this->ypos = ypos;
	this->visible = true;

	/** Load the image to use for the Sprite */
	this->load(filename, renderer);
}

Sprite::~Sprite()
{
	SDL_DestroyTexture(this->image);
}

void Sprite::load(const char* filename, SDL_Renderer *renderer)
{
	/** Load picture for Sprite */
	this->image = IMG_LoadTexture(renderer, filename);

	/** Check if the Sprite was loaded */
	if (this->image == NULL)
	{
		/** Bad stuff happened */
		cerr << "IMG_LoadTexture Error: " << IMG_GetError();
		exit(0);
	}
}

void Sprite::draw(SDL_Renderer *renderer)
{
	/** Check if sprite should be drawn to the screen */
	if (this->visible)
	{
		/** Position sprite on Screen */
		SDL_Rect dest = { this->xpos, this->ypos, this->rect.w, this->rect.h };

		/** Draw Sprite to Screen */
		SDL_RenderCopy(renderer, this->image, &this->rect, &dest);
	}
}

void Sprite::animate(Uint16 start_frame, Uint16 end_frame)
{
	Uint16 current_frame = this->getCurrentFrame();
	if (current_frame < start_frame || current_frame > end_frame)
		current_frame = start_frame;
	else{
		++current_frame;
		if (current_frame > end_frame)
			current_frame = start_frame;
	}
	this->setCurrentFrame(current_frame);
}

void Sprite::move(int deltaX, int deltaY)
{
	this->xpos += deltaX;
	this->ypos += deltaY;
}

void Sprite::setCurrentFrame(Uint16 frame)
{
	if (!this->rect.w)
		return;

	this->rect.x = this->rect.w * frame;
}

void Sprite::setRow(Uint16 row)
{
	if (!this->rect.h)
		return;

	this->rect.y = this->rect.h * row;
}

Uint16 Sprite::getRow()
{
	if (!this->rect.h)
		return 0;
	return
		this->rect.y / this->rect.h;
}

Uint16 Sprite::getCurrentFrame()
{
	if (!this->rect.w)
		return 0;
	return
		this->rect.x / this->rect.w;
}

SDL_Texture *Sprite::getImage()
{
	return this->image;
}

int Sprite::getWidth()
{
	return this->rect.w;
}

int Sprite::getHeight()
{
	return this->rect.h;
}

int Sprite::getXPos()
{
	return this->xpos;
}

int Sprite::getYPos()
{
	return this->ypos;
}

bool Sprite::isVisible()
{
	return this->visible;
}

void Sprite::setWidth(int width)
{
	this->rect.w = width;
}

void Sprite::setHeight(int height)
{
	this->rect.h = height;
}

void Sprite::setXPos(int x)
{
	this->xpos = x;
}

void Sprite::setYPos(int y)
{
	this->ypos = y;
}

void Sprite::setVisible(bool visible)
{
	this->visible = visible;
}